#include "traitements.h"
#include <time.h>

 int main() {
	 PPMImage *image;

	 /* Filtre Soft */
	 int filtreSoft[25] = { 0,0,0,0,0,
							0,1,3,1,0,
							0,3,5,3,0,
							0,1,3,1,0,
							0,0,0,0,0 };
	 int diviseurSoft = 25;

	 /* Filtre Sharpen (medium) */
	 int filtreSharpen[25] = { -1,-1,-1,-1,-1,
							   -1,-1,-1,-1,-1,
							   -1,-1,49,-1,-1,
							   -1,-1,-1,-1,-1,
							   -1,-1,-1,-1,-1 };
	 int diviseurSharpen = 25;

	 /* Filtre diagonal shatter */
	 int filtreDiag[25] = { 1,0,0,0,1,
							0,0,0,0,0,
							0,0,0,0,0,
							0,0,0,0,0,
							1,0,0,0,1 };
	 int diviseurDiag = 4;

	 /* Filtre Sobel */
	 int sobelH[25] = { 1,2,0,-2,-1,
						4,8,0,-8,-4,
						6,12,0,-12,-6,
						4,8,0,-8,-4,
						1,2,0,-2,-1 };
	 
	 int sobelV[25] = { -1,-4,-6,-4,-1,
						-2,-8,-12,-8,-2,
						0,0,0,0,0,
						2,8,12,8,2,
						1,4,6,4,1 };
	 int diviseurSobel = 1;


	 image = readPPM("gare_parallelisme2.ppm");

	 clock_t start, finish;
	 bool sobel = true;
	 int nbIterations = 1500;

	 start = clock();
	 if (sobel) {
		 convertToNB(image);
		 appliquerSeuil(image, 128);
	 }
	 for (int i = 0; i < nbIterations; i++) {
		 if (sobel)
			 image = applyFiltreSobel(image, sobelV, sobelH, diviseurSobel);
		 else
			 image = applyFiltre(image, filtreDiag, diviseurDiag);
	 }
	 finish = clock();

	 double time = finish - start;
	 printf("Temps pour %d iterations : %f\n",nbIterations, time/CLOCKS_PER_SEC);
	 //changeColorPPM(image);
	 writePPM("mon_image.ppm", image);	 getchar();}