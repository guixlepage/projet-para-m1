#include "traitements.h"

int main() {
	PPMImage *image;
	image = readPPM("gare_parallelisme2.ppm");
	int filtreSoft[25] = { 0,0,0,0,0, 0,1,3,1,0, 0,3,5,3,0, 0,1,3,1,0, 0,0,0,0,0 };
	
	int filtreSobelHorizontal[25] = { 1,2,0,-2,-1, 
									  4,8,0,-8,-4, 
									  6,12,0,-12,-6, 
									  4,8,0,-8,-4, 
									  1,2,0,-2,-1 };

	int filtreSobelVertical[25] = { -1,-4,-6,-4,-1,
									-2,-8,-12,-8,-2,
									0,0,0,0,0,
									2,8,12,8,2,
									1,4,6,4,1 };

	int diviseur = 1;
	int seuil = 128;
	//changeColorPPM(image);
	convertToNB(image);
	appliquerSeuil(image,seuil);
	image = applyFiltre(image, filtreSobelVertical, filtreSobelHorizontal, diviseur);
	writePPM("mon_image2.ppm", image);
	getchar();
}