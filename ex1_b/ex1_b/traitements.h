#ifndef TRAITEMENT_H
#define TRAITEMENT_H


#include <math.h>
#include <omp.h>
#include "ppm_lib.h"

void changeColorPPM(PPMImage *img);
void convertToNB(PPMImage *img);
void appliquerSeuil(PPMImage *img, int seuil);
PPMImage *applyFiltre(PPMImage *img, int *filtre, int diviseur);
PPMImage *applyFiltreSobel(PPMImage *img, int *filtreV, int *filtreH, int diviseur);

#endif // !TRAITEMENT_H