#include "ppm_lib.h"
#include <math.h>
#include <omp.h>

void changeColorPPM(PPMImage *img)
{
	if (img) {
		#pragma omp parallel for
		for (int i = 0; i<img->x*img->y; i++) {
			img->data[i].red = RGB_COMPONENT_COLOR - img->data[i].red;
			img->data[i].green = RGB_COMPONENT_COLOR - img->data[i].green;
			img->data[i].blue = RGB_COMPONENT_COLOR - img->data[i].blue;
		}
	}
}

void convertToNB(PPMImage *img) {
	if (img) {
		#pragma omp parallel for
		for (int i = 0; i < img->x * img->y; i++) {
			double color = 0.2126*img->data[i].red + 0.7152 * img->data[i].green + 0.0722 * img->data[i].blue;
			img->data[i].blue = img->data[i].red = img->data[i].green = color;
		}
	}
}

void appliquerSeuil(PPMImage *img, int seuil) {
	if (img) {
		#pragma omp parallel for
		for (int i = 0; i < img->x * img->y; i++) {
			if (img->data[i].blue <= seuil)
				img->data[i].blue = img->data[i].red = img->data[i].green = 0;
			else
				img->data[i].blue = img->data[i].red = img->data[i].green = 255;
		}
	}
}

PPMImage *applyFiltre(PPMImage *img, int *filtre, int diviseur) {
	
	PPMImage *outputImage;

	/* Allocation image */
	outputImage = (PPMImage *)malloc(sizeof(PPMImage));
	outputImage->x = img->x;
	outputImage->y = img->y;
	outputImage->data = (PPMPixel*)malloc(outputImage->x * outputImage->y * sizeof(PPMPixel));
	
	#pragma omp parallel for 
	for (int i = 2 * img->x + 2; i < img->x*(img->y - 2) - 2; i++) {
		for (int color = 0; color < 3; color++) {
			int modulo = i % img->x;
			int y = i / img->x;
			int x = i % img->x;
			int gridCounter, final;

			if (modulo == 0 && modulo == 1 && modulo == img->x - 2 && modulo == img->x - 1) 
				outputImage->data[y*outputImage->x + x] = img->data[y*outputImage->x + x];

			final = 0;
			gridCounter = 0; // reset some values
			for (int y2 = -2; y2 <= 2; y2++) {// and for each pixel around our
				for (int x2 = -2; x2 <= 2; x2++) // "hot pixel"...
				{ // Add to our running total
					if (color == 0)
						final += img->data[img->x*(y + y2) + x + x2].red * filtre[gridCounter];
					if (color == 1)
						final += img->data[img->x*(y + y2) + x + x2].green * filtre[gridCounter];
					if (color == 2)
						final += img->data[img->x*(y + y2) + x + x2].blue * filtre[gridCounter];
					// Go to the next value on the filter grid
					gridCounter++;
				}
				// and put it back into the right range
				final /= diviseur;

				if (color == 0)
					outputImage->data[y*outputImage->x + x].red = final;
				if (color == 1)
					outputImage->data[y*outputImage->x + x].green = final;
				if (color == 2)
					outputImage->data[y*outputImage->x + x].blue = final;
			}
		}
	}
	return outputImage;
}

PPMImage *applyFiltreSobel(PPMImage *img, int *filtreV, int *filtreH, int diviseur) {
	
	PPMImage *outputImage;

	/* Allocation image */
	outputImage = (PPMImage *)malloc(sizeof(PPMImage));
	outputImage->x = img->x;
	outputImage->y = img->y;
	outputImage->data = (PPMPixel*)malloc(outputImage->x * outputImage->y * sizeof(PPMPixel));

	//PPMPixel *tabPixel = (PPMPixel*)malloc(img->x * img->y * sizeof(PPMPixel));

	#pragma omp parallel for
	for (int i = 2 * img->x + 2; i < img->x*(img->y - 2) - 2; i++) {

		int gridCounter = 0, finalV = 0, finalH = 0;
		int modulo = i%img->x;
		int y = i / img->x;
		int x = i % img->x;

		if (modulo == 0 && modulo == 1 && modulo == img->x - 2 && modulo == img->x - 1) 
			outputImage->data[y*outputImage->x + x] = img->data[y*outputImage->x + x];
		
		for (int y2 = -2; y2 <= 2; y2++) {// and for each pixel around our
			for (int x2 = -2; x2 <= 2; x2++) // "hot pixel"...
			{ // Add to our running total
				finalV += img->data[img->x*(y + y2) + x + x2].green * filtreV[gridCounter];
				finalH += img->data[img->x*(y + y2) + x + x2].green * filtreH[gridCounter];
				// Go to the next value on the filter grid
				gridCounter++;
			}
		}

		// and put it back into the right range
		finalV /= diviseur;
		finalH /= diviseur;

		double norm = sqrt(finalV*finalV + finalH*finalH);
		if (norm > 255)
			norm = 255;
		if (norm < 0)
			norm = 0;

		outputImage->data[y*outputImage->x + x].green = outputImage->data[y*outputImage->x + x].blue = outputImage->data[y*outputImage->x + x].red = norm;
	}
	return outputImage;
}