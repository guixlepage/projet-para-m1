#include "traitements.cpp"
#include "ppm_lib.cpp"


 int main() {
	PPMImage *image;
	clock_t start, finish;
	 
	/* Filtre Soft */
	int filtreSoft[25] = { 0,0,0,0,0,
						0,1,3,1,0,
						0,3,5,3,0,
						0,1,3,1,0,
						0,0,0,0,0 };
	int diviseurSoft = 25;

	/* Filtre Sharpen (medium) */
	int filtreSharpen[25] = { -1,-1,-1,-1,-1,
							-1,-1,-1,-1,-1,
							-1,-1,49,-1,-1,
							-1,-1,-1,-1,-1,
							-1,-1,-1,-1,-1 };
	int diviseurSharpen = 25;

	/* Filtre diagonal shatter */
	int filtreDiag[25] = { 1,0,0,0,1,
						0,0,0,0,0,
						0,0,0,0,0,
						0,0,0,0,0,
						1,0,0,0,1 };
	int diviseurDiag = 4;

	/* Filtre Sobel */
	int sobelH[25] = { 1,2,0,-2,-1,
					4,8,0,-8,-4,
					6,12,0,-12,-6,
					4,8,0,-8,-4,
					1,2,0,-2,-1 };
	 
	int sobelV[25] = { -1,-4,-6,-4,-1,
					-2,-8,-12,-8,-2,
					0,0,0,0,0,
					2,8,12,8,2,
					1,4,6,4,1 };
	int diviseurSobel = 1;

	int nbIterations = 1;

	/* Lecture image */
	image = readPPM("gare_parallelisme2.ppm");

	/* Appliquer le filtre */
	bool sobel = false;
	if (sobel) {
		convertToNB(image);
		appliquerSeuil(image, 128);
	}
	start = clock();
	for (int i = 0; i < nbIterations; i++) {
		printf("It�ration n� : %d\n", i);
		if (!sobel)
			image = applyFiltre(image, filtreDiag, diviseurDiag);
		else {
			image = applyFiltreSobel(image, sobelV, sobelH, diviseurSobel);
		}
	}
	finish = clock();
	double time = finish - start;
	printf("Temps pour %d iterations : %f\n", nbIterations,time / CLOCKS_PER_SEC);

	/* �crire l'image */
	writePPM("mon_image.ppm", image);	printf("Complete.");	getchar();}