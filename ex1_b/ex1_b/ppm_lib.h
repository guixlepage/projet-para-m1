#ifndef PPM_LIB_H
#define PPM_LIB_H


#include <stdio.h>
#include <stdlib.h>
#include <time.h>



#define RGB_COMPONENT_COLOR 255

typedef struct {
     unsigned char red,green,blue;
} PPMPixel;

typedef struct {
     int x, y;
     PPMPixel *data;
} PPMImage;

PPMImage *readPPM(const char *filename);
void writePPM(const char *filename, PPMImage *img);

void changeColorPPM(PPMImage *img);
void convertToNB(PPMImage *img);
void appliquerSeuil(PPMImage *img, int seuil);
PPMImage *applyFiltre(PPMImage *img, int *filtre, int diviseur);
PPMImage *applyFiltreSobel(PPMImage *img, int *filtreV, int *filtreH, int diviseur);


#endif // !PPM_LIB_H