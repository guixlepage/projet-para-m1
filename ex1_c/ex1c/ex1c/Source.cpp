#include <stdio.h>
#include <stdlib.h>
#include "traitements.h"
#include "Kernel.h"
#include <iostream>
#include <time.h>

int main()
{

	bool sobel = true;
	int nbr_iteration = 15;
	PPMImage *image;
	image = readPPM("gare_parallelisme2.ppm");

	/* Filtre Soft */
	int filtreSoft[25] = { 0,0,0,0,0,
		0,1,3,1,0,
		0,3,5,3,0,
		0,1,3,1,0,
		0,0,0,0,0 };
	int diviseurSoft = 25;

	/* Filtre Sharpen (medium) */
	int filtreSharpen[25] = { -1,-1,-1,-1,-1,
		-1,-1,-1,-1,-1,
		-1,-1,49,-1,-1,
		-1,-1,-1,-1,-1,
		-1,-1,-1,-1,-1 };
	int diviseurSharpen = 25;

	/* Filtre diagonal shatter */
	int filtreDiag[25] = { 1,0,0,0,1,
		0,0,0,0,0,
		0,0,0,0,0,
		0,0,0,0,0,
		1,0,0,0,1 };
	int diviseurDiag = 4;

	/* Filtre Sobel */
	int sobelH[25] = { 1,2,0,-2,-1,
		4,8,0,-8,-4,
		6,12,0,-12,-6,
		4,8,0,-8,-4,
		1,2,0,-2,-1 };

	int sobelV[25] = { -1,-4,-6,-4,-1,
		-2,-8,-12,-8,-2,
		0,0,0,0,0,
		2,8,12,8,2,
		1,4,6,4,1 };
	int diviseurSobel = 1;

	float temps;
	clock_t t1, t2;

	t1 = clock();


		if (!sobel) {
			PPMPixel *pixels = appelKernel(image, filtreDiag, diviseurDiag, nbr_iteration);
			PPMImage *imageReturn = new PPMImage;
			imageReturn->x = image->x;
			imageReturn->y = image->y;
			imageReturn->data = pixels;
			image = imageReturn;
			writePPM("mon_image.ppm", image);
		}
		else
		{
			image = appelLuminance(image);
			
			PPMPixel *pixels = appelSobel(image, sobelH, sobelV, diviseurSobel, nbr_iteration);

			PPMImage *imageReturn = new PPMImage;
			imageReturn->x = image->x;
			imageReturn->y = image->y;
			imageReturn->data = pixels;
			image = imageReturn;
			writePPM("mon_image_sobel.ppm", image);
		}
	
	t2 = clock();
	temps = (float)(t2 - t1) / CLOCKS_PER_SEC;
	printf("Temps execution total pour %d iterations : %f sec ou %f ms \n",nbr_iteration, temps, temps*1000);

	return 0;
}