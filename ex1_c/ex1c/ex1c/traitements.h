#include <cstdio>
#include <cstdlib>
#include <math.h>
#include "ppm_lib.h"

void changeColorPPM(PPMImage *img);
void convertToNB(PPMImage *img);
void appliquerSeuil(PPMImage *img, int seuil);
PPMImage *applyFiltre(PPMImage *img, int *filtre, int diviseur);
PPMImage *applyFiltreSobel(PPMImage *img, int *filtreV, int *filtreH, int diviseur);