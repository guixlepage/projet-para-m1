#pragma once
#include <stdio.h>
#include "traitements.h"
#include "cuda.h"
#include "cuda_runtime.h"

PPMPixel* appelKernel(PPMImage *image, int filtre[25], int diviseur, int iteration);
PPMImage* appelLuminance(PPMImage *image);
PPMPixel* appelSobel(PPMImage *image, int filtreH[25], int filtreV[25], int diviseur, int iteration);
