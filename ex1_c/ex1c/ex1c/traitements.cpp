#include "traitements.h"

void changeColorPPM(PPMImage *img)
{
	int i;
	if (img) {
		for (i = 0; i<img->x*img->y; i++) {
			img->data[i].red = RGB_COMPONENT_COLOR - img->data[i].red;
			img->data[i].green = RGB_COMPONENT_COLOR - img->data[i].green;
			img->data[i].blue = RGB_COMPONENT_COLOR - img->data[i].blue;
		}
	}
}

void convertToNB(PPMImage *img) {
	for (int y = 0; y < img->y; y++) {
		for (int x = 0; x < img->x; x++) {
			double color = 0.2126*img->data[y*img->x + x].red + 0.7152 * img->data[y*img->x + x].green + 0.0722 * img->data[y*img->x + x].blue;
			img->data[y*img->x + x].blue = img->data[y*img->x + x].red = img->data[y*img->x + x].green = color;
		}
	}
	printf("NB complete.");
}

void appliquerSeuil(PPMImage *img, int seuil) {
	for (int y = 0; y < img->y; y++) {
		for (int x = 0; x < img->x; x++) {
			if (img->data[y*img->x + x].blue <= seuil)
				img->data[y*img->x + x].blue = img->data[y*img->x + x].red = img->data[y*img->x + x].green = 0;
			else
				img->data[y*img->x + x].blue = img->data[y*img->x + x].red = img->data[y*img->x + x].green = 255;
		}
	}
	printf("Seuil complete.");
}

PPMImage *applyFiltre(PPMImage *img, int *filtre, int diviseur) {
	int gridCounter, final;
	PPMImage *outputImage;

	/* Allocation image */
	outputImage = (PPMImage *)malloc(sizeof(PPMImage));
	outputImage->x = img->x - 4;
	outputImage->y = img->y - 4;
	outputImage->data = (PPMPixel*)malloc(outputImage->x * outputImage->y * sizeof(PPMPixel));
	for (int color = 0; color < 3; color++) {
		for (int y = 2; y < img->y - 2; y++) { // for each pixel in the image
			for (int x = 2; x < img->x - 2; x++)
			{
				final = 0;
				gridCounter = 0; // reset some values
				for (int y2 = -2; y2 <= 2; y2++) {// and for each pixel around our
					for (int x2 = -2; x2 <= 2; x2++) // "hot pixel"...
					{ // Add to our running total
						//printf("valeur pixel : %d\n", filtre[gridCounter]);
						if (color == 0)
							final += img->data[img->x*(y + y2) + x + x2].red * filtre[gridCounter];
						if (color == 1)
							final += img->data[img->x*(y + y2) + x + x2].green * filtre[gridCounter];
						if (color == 2)
							final += img->data[img->x*(y + y2) + x + x2].blue * filtre[gridCounter];
						// Go to the next value on the filter grid
						gridCounter++;
					}
				}

				// and put it back into the right range
				final /= diviseur;

				//printf("%d\n", y*outputImage->x + x);
				if (color == 0)
					outputImage->data[(y - 2)*outputImage->x + (x - 2)].red = final;
				if (color == 1)
					outputImage->data[(y - 2)*outputImage->x + (x - 2)].green = final;
				if (color == 2)
					outputImage->data[(y - 2)*outputImage->x + (x - 2)].blue = final;
			}
		}
	}
	printf("Filtre complete.");
	return outputImage;
}

PPMImage *applyFiltreSobel(PPMImage *img, int *filtreV, int *filtreH, int diviseur) {
	convertToNB(img);
	appliquerSeuil(img,128);
	int gridCounter, finalV, finalH;
	PPMImage *outputImage;

	/* Allocation image */
	outputImage = (PPMImage *)malloc(sizeof(PPMImage));
	outputImage->x = img->x - 4;
	outputImage->y = img->y - 4;
	outputImage->data = (PPMPixel*)malloc(outputImage->x * outputImage->y * sizeof(PPMPixel));

	for (int y = 2; y < img->y - 2; y++) { // for each pixel in the image
		for (int x = 2; x < img->x - 2; x++)
		{
			gridCounter = 0; // reset some values
			finalV = 0;
			finalH = 0;
			for (int y2 = -2; y2 <= 2; y2++) {// and for each pixel around our
				for (int x2 = -2; x2 <= 2; x2++) // "hot pixel"...
				{ // Add to our running total

				  //printf("valeur pixel : %d\n", filtre[gridCounter]);
					finalV += img->data[img->x*(y + y2) + x + x2].green * filtreV[gridCounter];
					finalH += img->data[img->x*(y + y2) + x + x2].green * filtreH[gridCounter];
					// Go to the next value on the filter grid
					gridCounter++;
				}
			}

			// and put it back into the right range
			finalV /= diviseur;
			finalH /= diviseur;

			float norm = sqrt(finalV*finalV + finalH*finalH);
			if (norm > 255)
				norm = 255;
			if (norm < 0)
				norm = 0;

			//printf("%d\n", y*outputImage->x + x);
			outputImage->data[(y - 2)*outputImage->x + (x - 2)].green = outputImage->data[(y - 2)*outputImage->x + (x - 2)].blue = outputImage->data[(y - 2)*outputImage->x + (x - 2)].red = norm;
		}
	}
	printf("Sobel complete.");
	return outputImage;
}