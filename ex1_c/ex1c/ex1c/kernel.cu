#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cuda.h>
#include <device_functions.h>
#include <cuda_runtime_api.h>
#include "Kernel.h"
#include <math.h>

static void HandleError(cudaError_t err, const char *file, int line) {
	if (err != cudaSuccess)
	{
		printf("%s in %s at line %d\n", cudaGetErrorString(err), file, line); exit(EXIT_FAILURE);
	}
}
#define HANDLE_ERROR(err) (HandleError(err, __FILE__, __LINE__))

__global__ void luminance(PPMPixel *tabOrigin, PPMPixel *tabReturn, int height, int width) {
	//applique une luminance et un seuillage qui sont utile pour le filtre de sobel!

	int row = blockDim.y * blockIdx.y + threadIdx.y;
	int col = blockDim.x * blockIdx.x + threadIdx.x;

	int val = (tabOrigin[row*width + col].red * 0.2126)+ (tabOrigin[row*width + col].green * 0.7152)+ (tabOrigin[row*width + col].blue * 0.0722);
	
	if (val > 127)
	{
		val = 255;
	}
	if (val < 127) {
		val = 0;
	}
	
	tabReturn[row*width + col].red = val;
	tabReturn[row*width + col].green = val;
	tabReturn[row*width + col].blue = val;
}

__global__ void filtreSobel(PPMPixel *tabOrigin, PPMPixel *tabOrigin1, PPMPixel *tabReturn, int height, int width)
{
	//Prend les deux images apr�s applications des noyaux respectifs et va permettre de choisir la valeur finale
	int row = blockDim.y * blockIdx.y + threadIdx.y;
	int col = blockDim.x * blockIdx.x + threadIdx.x;

	float val = (tabOrigin[row*width + col].red * tabOrigin[row*width + col].red) + (tabOrigin1[row*width + col].red * tabOrigin1[row*width + col].red);
	val = sqrt(val);

	if(val > 255){
		val = 255;
	}
	if (val < 0){
		val = 0;
	}

	tabReturn[row*width + col].red = val;
	tabReturn[row*width + col].green = val;
	tabReturn[row*width + col].blue = val;
}

__global__ void transfertKernel(PPMPixel *tabOrigin, PPMPixel *tabReturn, int height, int width) {
	int row = blockDim.y * blockIdx.y + threadIdx.y;
	int col = blockDim.x * blockIdx.x + threadIdx.x;
	tabOrigin[row*width + col] = tabReturn[row*width + col];
}

__global__ void filtreKernel(PPMPixel *tabOrigin, PPMPixel *tabReturn, int *noyau, int diviseur, int height, int width)
{
	//applique un noyau de convolution de 5x5
	//int threadId = threadIdx.x; // index du thread dans le bloc,
	//int blockDim = blockDim;  // nombre de threads par bloc (valeur de threadsParBloc du param�trage du kernel).
	//uint3 blockId = blockIdx; //index du bloc dans la grille

	int taille_noyau = 5;
	int marge = 2; //largeur filtre /2 
	int row = blockDim.y * blockIdx.y + threadIdx.y;
	int col = blockDim.x * blockIdx.x + threadIdx.x;

/*	tabReturn[row*width + col].red = tabOrigin[row*width + col].red;
	tabReturn[row*width + col].green = tabOrigin[row*width + col].green;
	tabReturn[row*width + col].blue = tabOrigin[row*width + col].blue;*/


	 if ((row > 2) && (col < width - 2) && (col > 2) && (row < height - 2)) {

		int ligne = 0;
		int colonne = 0;
		int sumRed = 0;
		int sumGreen = 0;
		int sumBlue = 0;



	for (int l = col - marge; l < col - marge + taille_noyau; l++) { //on se d�place dans le noyau sur l'image
			for (int m = row - marge; m < row - marge + taille_noyau; m++) { //on se d�place dans le noyau sur l'image
				double multi = noyau[colonne * 5 + ligne];
				double valImageRed = tabOrigin[m*width + l].red;
				double valImageGreen = tabOrigin[m*width + l].green;
				double valImageBlue = tabOrigin[m*width + l].blue;
				sumRed += valImageRed * multi;
				sumGreen += valImageGreen * multi;
				sumBlue += valImageBlue * multi;
				//on d�place dans le noyau
				colonne += 1;
				if (colonne >= taille_noyau) {
					ligne += 1;
					colonne = 0;
				}
				if (ligne >= taille_noyau)ligne = 0;
			}
		}

		tabReturn[row*width + col].red = sumRed / diviseur;
		tabReturn[row*width + col].green = sumGreen / diviseur;
		tabReturn[row*width + col].blue = sumBlue / diviseur;
	} 

	//sum += tabOrigin[row*width + col].blue * noyau[row*width + col];


	//tabReturn[row*width + col] = tabOrigin[row*width + col];

	//printf("Je suis la row %d et la col %d \n", row,col); //fait planter, c'est comme un chinois, ca bosse pas vite.

	//printf("L'image fait %d par %d", row, col);
	//image[i * width + j]  -> case en i j 

    
}

PPMPixel* appelKernel(PPMImage *image, int filtre[25], int diviseur,int iteration){
	int size = image->x * image->y;

	int width = image->x; //col
	int height = image->y; //lig

	PPMPixel *tabPixels = 0;	
	PPMPixel *tabOrigin = 0;
	PPMPixel *tabReturn = 0;

	tabPixels = image->data; //tabPixel est le tableau de pixel donn� au GPU
	PPMPixel *pixelsTemp = (PPMPixel*)malloc(size * sizeof(PPMPixel));  //pour r�cup�rer les valeurs
	PPMPixel *pixelsReturn = (PPMPixel*)malloc(size * sizeof(PPMPixel));  //tableau vide pour �crire l'image dans le gpu
	int *noyau = 0;
	


	HANDLE_ERROR (cudaMalloc((void**)&noyau, 25 * sizeof(int)));
	HANDLE_ERROR (cudaMalloc((void**)&tabOrigin, size * sizeof(PPMPixel)));
	HANDLE_ERROR(cudaMalloc((void**)&tabReturn, size * sizeof(PPMPixel)));

	HANDLE_ERROR (cudaMemcpy(noyau, filtre, 25 * sizeof(int), cudaMemcpyHostToDevice));
	HANDLE_ERROR (cudaMemcpy(tabOrigin, tabPixels, size * sizeof(PPMPixel), cudaMemcpyHostToDevice));

	// Launch a kernel on the GPU with one thread for each element.

	cudaEvent_t start, stop;
	float time;

	cudaEventCreate(&start);

	cudaEventCreate(&stop);

	cudaEventRecord(start, 0);

	
	dim3 blockDim(image->x/16, image->y/16);

	dim3 ThreadPerlock(16,16);

	for (int i = 0; i < iteration; i++) {
		filtreKernel << < blockDim, ThreadPerlock >> >(tabOrigin, tabReturn, noyau, diviseur, height, width);
		//myKernel<<< dimGrid, dimBlock[, dimMem ]>>>(params);
		cudaDeviceSynchronize();
		transfertKernel << < blockDim, ThreadPerlock >> >(tabOrigin, tabReturn, height, width);
		cudaDeviceSynchronize();
	}


	printf(">%s\n", cudaGetErrorString(cudaGetLastError()));  //Affiche si erreur dans CUDA 

	cudaEventRecord(stop, 0);

	cudaEventSynchronize(stop);

	cudaEventElapsedTime(&time, start, stop);

	printf("Temps necessaire :  %3.10f ms \n", time);

	HANDLE_ERROR(cudaMemcpy(pixelsTemp, tabReturn, size * sizeof(PPMPixel), cudaMemcpyDeviceToHost));

	return pixelsTemp;
}


PPMPixel* appelSobel(PPMImage *image, int filtreH[25], int filtreV[25], int diviseur, int iteration) {
	int size = image->x * image->y;

	int width = image->x; //col
	int height = image->y; //lig

	PPMPixel *tabPixels = 0;
	PPMPixel *tabOrigin = 0;
	PPMPixel *tabReturn = 0;
	PPMPixel *imageH = 0;
	PPMPixel *imageV = 0;

	tabPixels = image->data; //tabPixel est le tableau de pixel donn� au GPU
	PPMPixel *pixelsTemp = (PPMPixel*)malloc(size * sizeof(PPMPixel));  //pour r�cup�rer les valeurs
	int *noyauH = 0;
	int *noyauV = 0;

	HANDLE_ERROR(cudaMalloc((void**)&noyauH, 25 * sizeof(int)));
	HANDLE_ERROR(cudaMalloc((void**)&noyauV, 25 * sizeof(int)));
	HANDLE_ERROR(cudaMalloc((void**)&tabOrigin, size * sizeof(PPMPixel)));
	HANDLE_ERROR(cudaMalloc((void**)&tabReturn, size * sizeof(PPMPixel)));
	HANDLE_ERROR(cudaMalloc((void**)&imageH, size * sizeof(PPMPixel)));
	HANDLE_ERROR(cudaMalloc((void**)&imageV, size * sizeof(PPMPixel)));

	HANDLE_ERROR(cudaMemcpy(tabOrigin, tabPixels, size * sizeof(PPMPixel), cudaMemcpyHostToDevice));
	HANDLE_ERROR(cudaMemcpy(noyauH, filtreH, 25 * sizeof(int), cudaMemcpyHostToDevice));
	HANDLE_ERROR(cudaMemcpy(noyauV, filtreV, 25 * sizeof(int), cudaMemcpyHostToDevice));

	// Launch a kernel on the GPU with one thread for each element.

	cudaEvent_t start, stop;
	float time;

	cudaEventCreate(&start);

	cudaEventCreate(&stop);

	cudaEventRecord(start, 0);

	dim3 blockDim(image->x / 16, image->y / 16);

	dim3 ThreadPerlock(16, 16);

	for (int i = 0; i < iteration; i++) {
		//Bloc limit� par la carte, on fait le plus de bloc possible de 512 threads
		filtreKernel << < blockDim, ThreadPerlock >> >(tabOrigin, imageH, noyauH, diviseur, height, width);
		cudaDeviceSynchronize();
		filtreKernel << < blockDim, ThreadPerlock >> >(tabOrigin, imageV, noyauV, diviseur, height, width);
		//myKernel<<< dimGrid, dimBlock[, dimMem ]>>>(params);
		cudaDeviceSynchronize();
		filtreSobel<< < blockDim, ThreadPerlock >> >(imageH, imageV, tabReturn, height, width);
		cudaDeviceSynchronize();
		transfertKernel << < blockDim, ThreadPerlock >> >(tabOrigin, tabReturn, height, width);

	}


	printf(">%s\n", cudaGetErrorString(cudaGetLastError()));  //Affiche si erreur dans CUDA 

	cudaEventRecord(stop, 0);

	cudaEventSynchronize(stop);

	cudaEventElapsedTime(&time, start, stop);

	printf("Temps necessaire :  %3.10f ms \n", time);

	HANDLE_ERROR(cudaMemcpy(pixelsTemp, tabReturn, size * sizeof(PPMPixel), cudaMemcpyDeviceToHost));

	return pixelsTemp;
}


PPMImage* appelLuminance(PPMImage *image) {
	int size = image->x * image->y;

	int width = image->x; //col
	int height = image->y; //lig

	PPMPixel *tabPixels = 0;
	PPMPixel *tabOrigin = 0;
	PPMPixel *tabReturn = 0;

	tabPixels = image->data; //tabPixel est le tableau de pixel donn� au GPU
	PPMPixel *pixelsTemp = (PPMPixel*)malloc(size * sizeof(PPMPixel));  //pour r�cup�rer les valeurs
	PPMPixel *pixelsReturn = (PPMPixel*)malloc(size * sizeof(PPMPixel));  //tableau vide pour �crire l'image dans le gpu

	HANDLE_ERROR(cudaMalloc((void**)&tabOrigin, size * sizeof(PPMPixel)));
	HANDLE_ERROR(cudaMalloc((void**)&tabReturn, size * sizeof(PPMPixel)));

	HANDLE_ERROR(cudaMemcpy(tabOrigin, tabPixels, size * sizeof(PPMPixel), cudaMemcpyHostToDevice));
	HANDLE_ERROR(cudaMemcpy(tabReturn, pixelsReturn, size * sizeof(PPMPixel), cudaMemcpyHostToDevice));

	// Launch a kernel on the GPU with one thread for each element.

	cudaEvent_t start, stop;
	float time;

	cudaEventCreate(&start);

	cudaEventCreate(&stop);

	cudaEventRecord(start, 0);


	dim3 blockDim(image->x / 16, image->y / 16);

	dim3 ThreadPerlock(16, 16);

	//Bloc limit� par la carte, on fait le plus de bloc possible de 512 threads
	luminance << < blockDim, ThreadPerlock >> >(tabOrigin, tabReturn, height, width);
	//myKernel<<< dimGrid, dimBlock[, dimMem ]>>>(params);

	cudaDeviceSynchronize();

	HANDLE_ERROR(cudaMemcpy(pixelsTemp, tabReturn, size * sizeof(PPMPixel), cudaMemcpyDeviceToHost));

	PPMImage *imageReturn = new PPMImage;
	imageReturn->x = image->x;
	imageReturn->y = image->y;
	imageReturn->data = pixelsTemp;
	return imageReturn;

}


